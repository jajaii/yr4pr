function [CointConst_a,CointPrem_delta,CointVal_alpha,a0] = ParaEst(Yem)
%CID:01069485

mm                              = min(size(Yem));
LL                              = max(size(Yem));

log_Data                        = log(Yem);
Observable_Y                    = log_Data(1,:)';
DesignMatrix_X                  = log_Data(2:mm,:)';

%TT = linspace(0,1,LL).*-TIME;
%XX = [TT;XX]';

Para                            = (DesignMatrix_X'*DesignMatrix_X)^-1 *(DesignMatrix_X' * Observable_Y);

CointConst_a                    = [1;-Para];
CointVal_alpha                  = (CointConst_a'*log_Data)';

a0                              = 0;

MeanCointVal_alpha              = mean(CointVal_alpha);

Diff_Y_Data                     = diff(Yem');
Y_Data_Post                     = Yem(:,2:LL);
CointPrem_delta                 = mean((Diff_Y_Data'./Y_Data_Post),2)/(MeanCointVal_alpha*(1/LL));
CointPrem_delta                 = CointPrem_delta ./ CointPrem_delta(1);
end
