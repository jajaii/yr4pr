function [plm,piem,alphaem,profit] = ShoAPec(a,a0,delta,Yem,omega,Dt,commision,Interval)
%CID:01069485
LL                  = max(size(Yem));
L                   = min(size(Yem));
A                   = diag(a);
Gamma               = 100;
Ytemp               = Yem(:,1);
alphatemp           = a0 + a'*log(Ytemp);
piem                = zeros(L,LL);
plm                 = zeros(L,LL);
alphaem             = zeros(1,LL);
alphaem(1)          = alphatemp;
PosChange           = zeros(L,LL);
PosChange(:,1)      = plm(:,1);
ComCost             = zeros(L,LL);
PChange             = zeros(L,LL);
PChange(:,2:end)    = diff(Yem')' ./ Yem(:,1:end-1);
PL                  = zeros(L,LL);

gu                  = 100; 

pitemp              = gu*(1/Gamma)*(((omega^(-1))*delta)*alphatemp - a*((delta'*(omega^(-1))*delta)*(LL/Interval*Dt*alphatemp - 0.25*trace(A*omega)*(LL/Interval*Dt)^2)));
Vol_Pos_prior       = pitemp./Ytemp;
% Vol_Pos_prior       = (Vol_Pos_prior./abs(Vol_Pos_prior(1)))*10000;
Proposed_invest     = sum(abs(piem(:,1)));


commission_fee      = Proposed_invest*commision;
PL(:,1)             = - commission_fee; %0.0013;
piem(:,1)           = pitemp;
for i = 2:LL
    Ytemp           = Yem(:,i);
    alphatemp       = a0 + a'*log(Ytemp);
    
    if mod(i-1,Interval) == 0
        pitemp          = gu*(1/Gamma)*(((omega^(-1))*delta)*alphatemp - a*((delta'*(omega^(-1))*delta)*(((LL-i+1)/Interval *Dt)*alphatemp - 0.25*trace(A*omega)*((LL-i+1)/Interval *Dt)^2)));
        Vol_Pos_post    =  pitemp./Ytemp;
%         Vol_Pos_post    = (Vol_Pos_post./abs(Vol_Pos_post(1)))*10000;
        Vol_Pos_prior   =  Vol_Pos_post;
    end
    

    piem(:,i)       = pitemp;
    plm(:,i)        = Vol_Pos_prior;
    PosChange(:,i)  = plm(:,i) - plm(:,i-1);   %Mistake here.
    
    Proposed_invest = abs( PosChange(:,i) ).*Yem(:,i);
    ComCost(:,i)    = Proposed_invest  * commision; %Mistake here.
    PL(:,i)         = piem(:,i-1).*PChange(:,i);
    

    alphaem(i)      = alphatemp;
    
end
PL                  = PL - ComCost;
profit              = cumsum(sum(PL));
% plot(cumsum(sum(PL)));
end