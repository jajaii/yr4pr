% close all;
%CID:01069485
ly = log(Yem);
YYY = ly(1,:);
mm = min(size(Yem));
LL = max(size(Yem));

%TT = linspace(0,1,LL).*-TIME; 
XX = ly(2:mm,:);
%XX = [TT;XX]';
XX = XX';
YYY = YYY';
bc = mvregress(XX,YYY);
boc = (XX'*XX)^-1 *(ones(mm-1,1).*2500 + XX' * YYY);
disp('bc =');disp(bc)
disp('boc =');disp(boc)
aboc = [1;-boc];
apple = [1;-bc];
%alph = (apple'*[TT;log(Yem)])';
alpboc = (aboc'*[log(Yem)])';
alph = (apple'*[log(Yem)])';

amean = mean(alph);

yoi = diff(Yem');
yop = Yem(:,2:LL);
delta = mean((yoi'./yop),2)/amean;
delta = delta ./ delta(1);
% 
% 
% figure()
% plot(boc' * XX'),hold on
% plot(bc' * XX'),hold off
% legend

figure()
plot(alphaem),hold on
plot(-alph),hold on
plot(-alpboc),hold off
legend
