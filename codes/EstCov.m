function EstCovar = EstCov(Yem,Dt)
%Yem: Data
%Dt : Trading interval

Log_Yem                     = log(Yem);
first_order_log_diff        = diff(Log_Yem,1,1);
EstCovar                      = cov(first_order_log_diff)/Dt;

end