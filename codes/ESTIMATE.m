%CID:01069485
ly = log(Yem);
YYY = ly(1,:);
XX = ly(2:3,:);
XX = XX';
YYY = YYY';
TTT = [0:Dt:T]';
XXX = [TTT,XX];
ONE = ones(length(TTT),1);
XXXX = [ONE,XX];
[bc,sig,EW,CB,lL] = mvregress(XX,YYY);
[boi,~,~,~,~] = mvregress(XXXX,YYY);
aplo = [boi(1),1,-boi(2),-boi(3)];
apple = [1;-bc];
alph = (apple'*log(Yem))';

ya1 = diff(ly(1,:))./(ly(1,2:2049));
yo1 = alph(2:2049);
[bs1,sig1s,EW1s,CB1s,lL1s] = mvregress(ya1',yo1);

ya2 = diff(ly(2,:))./(ly(2,2:2049));
yo2 = alph(2:2049);
[bs2,sig2s,EW2s,CB2s,lL2s] = mvregress(ya2',yo2);

ya3 = diff(ly(3,:))./(ly(3,2:2049));
yo3 = alph(2:2049);
[bs3,sig3s,EW3s,CB3s,lL3s] = mvregress(ya3',yo3);




poi = [Yem',alph]; 
mdl = varm(3,1);
estmdl = estimate(mdl,Yem');
Yco = estmdl.Constant;
YB = estmdl.AR{1,1};
Yvar = estmdl.Covariance;

%[~,~,~,~,reg11,reg12] = egcitest([diff(poi(:,1))./poi(2:2049,1),(poi(1:2048,4))*Dt],'creg','nc','lags',0,'alpha',0.05);
%[~,~,~,~,reg21,reg22] = egcitest([diff(poi(:,2))./poi(2:2049,2),(poi(1:2048,4))*Dt],'creg','nc','lags',0,'alpha',0.05);
%[~,~,~,~,reg31,reg32] = egcitest([diff(poi(:,3))./poi(2:2049,3),(poi(1:2048,4))*Dt],'creg','nc','lags',0,'alpha',0.05);
[~,p1,~,~,reg11] = egcitest([log(poi(:,1)),poi(:,4)],'creg','nc','lags',0,'alpha',0.05);
[~,p2,~,~,reg21] = egcitest([log(poi(:,2)),poi(:,4)],'creg','nc','lags',0,'alpha',0.05);
[~,p3,~,~,reg31] = egcitest([log(poi(:,3)),poi(:,4)],'creg','nc','lags',0,'alpha',0.05);
%[~,~,~,~,reg51,reg52] = egcitest([poi(:,4),log(poi(:,3))],'creg','nc','lags',0,'alpha',0.05);
[~,~,~,~,reg41,reg42] = egcitest(log(Yem'),'creg','nc','lags',0,'alpha',0.05);

co1 = reg11.coeff;
co2 = reg21.coeff;
co3 = reg31.coeff;

%k1 = mvregress(diff(log(poi(:,1))),co1*poi(1:2048,4));
%k2 = mvregress(diff(log(poi(:,2))),co2*poi(1:2048,4));
%k3 = mvregress(diff(log(poi(:,3))),co3*poi(1:2048,4));

q = 1;
[numObs,numDims] = size(log(Yem'));
tBase = (q+2):numObs;                    % Commensurate time base, all lags
Tl = length(tBase);                       % Effective sample size
YLags = lagmatrix(log(Yem'),0:(q+1));            % Y(t-k) on observed time base
LY = YLags(tBase,(numDims+1):2*numDims); % Y(t-1) on commensurate time base
DeltaYLags = zeros(Tl,(q+1)*numDims);
for k = 1:(q+1)
    DeltaYLags(:,((k-1)*numDims+1):k*numDims) = ...
               YLags(tBase,((k-1)*numDims+1):k*numDims) ...
             - YLags(tBase,(k*numDims+1):(k+1)*numDims);
end
 
DY = DeltaYLags(:,1:numDims);        % (1-L)Y(t)
DLY = DeltaYLags(:,(numDims+1):end); % [(1-L)Y(t-1),...,(1-L)Y(t-q)]
Xh = [(LY*beta-boi(1)),DLY,ones(Tl,1)]; 
P = ((Xh'*Xh)^-1 * Xh' * DY)'; % [alpha,B1,...,Bq,c1]
alphaaaa = P(:,1);


bla = aplo(1)+aplo(2:4)*log(Yem);
blo = apple'*log(Yem);
noob = alphaaaa.*bla;
newbee = alphaaaa.*blo;

figure()
plot(TTT,apple'*log(Yem))