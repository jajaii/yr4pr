% plm is the number of shares to hold at each time point
% piem is the total to invest at each time point
 
% Want to calculate Profit/Loss with commission
 
% Calculate change in number of shares held
PosChange      = zeros(min(size(Yem)),LL);
PosChange(:,1) = plm(:,1);
for i = 2:LL
    PosChange(:,i) = plm(:,i) - plm(:,i-1);
end
 
% Calculate commission based on cost of shares traded
ComCost = zeros(min(size(Yem)),LL);
for i = 1:LL
    ComCost(:,i) = abs( PosChange(:,i)'*Yem(:,i) ) * commision;
end
 
% Calculate percent change in prices
PChange          = zeros(min(size(Yem)),LL);
PChange(:,2:end) = diff(Yem')' ./ Yem(:,1:end-1);
 
% Calculate profit made
PL      = zeros(min(size(Yem)),LL);
for i = 2:LL
    % Calculate PL change since last time point
    PL(:,i) = piem(:,i-1).*PChange(:,i);
end
 
% Now subtract commission from profit and loss
PL = PL - ComCost;
 
% Plot the cumulative sum of the individual profits and losses
plot(cumsum(sum(PL)));
pause


