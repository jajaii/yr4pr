function PLOT(Yem,T,WinerProc,a1,a0,L)
%CID:01069485



TTT = linspace(0,T,L+1);
figure

%Plot for simulated Data
subplot(3,1,1)
for l = 1:length(Yem(:,1))
    plot(TTT,[Yem(l,:)]),hold on
end
hold off
xlabel('Time','FontSize',12);
ylabel('Value','FontSize',12,'Rotation',90);
title('Asset Price Processes')


%Plot for estimated cointegration series alpha
subplot(3,1,2)
plot(TTT,a0+a1'*log(Yem));
xlabel('Time','FontSize',12);
ylabel('\alpha','FontSize',12,'Rotation',90);
title('Estimated Coint Series')

%Plot for simulated no drift data
subplot(3,1,3)
for l = 1:length(WinerProc(:,1))
    plot(TTT,[WinerProc(l,:)]),hold on
end
hold off
xlabel('Time','FontSize',12);
ylabel('Value','FontSize',12,'Rotation',90);
title('No Drift GBM white noise Process')

end