function [model_cov_correction,logmodel_cov_correction,model_eigval,model_eigvec,model_eigvec_inv,mean_estimation,lincomb_coeff,standard_deviation,logmodel_eigvec_inv,logmodel_eigval,model_A,model_B,model_cov] = VARMOD(Yem,Dt)
%CID:01069485
%logmodel                : log var model initiation 
%logmodel_estimation     : log var model estimation 
%logmodel_B              : B of Yt = A + B*Yt-1 + noise
%logmodel_cov            : estimated variance-covariance matrix from the model
%logmodel_cov_correction : adjusted final variance-covariance matrix of the data
%logmodel_kappa          : estimated kappa value of mean reverting
%logmodel_eigvec         : eigenvector of logmodel_kappa
%logmodel_eigval         : eigenvalues of logmodel_kappa
%logmodel_eigvec_inv     : inverse of logmodel_eigvec


logmodel                  = varm(length(Yem(:,1)),1);
logmodel_estimation       = estimate(logmodel,log(Yem'));
logmodel_B                = logmodel_estimation.AR{1,1};
logmodel_cov              = logmodel_estimation.Covariance;
logmodel_cov_correction   = logmodel_cov/Dt;

logmodel_kappa                          = (eye(length(Yem(:,1)))-logmodel_B)/Dt;
[logmodel_eigvec,logmodel_eigval]       = eig(logmodel_kappa);
logmodel_eigvec_inv                     = logmodel_eigvec^-1;


%following variables naming convention follow previous ones without the 'lo'
model                       = varm(length(Yem(:,1)),1);
model_estimation            = estimate(model,Yem');
model_B                     = model_estimation.AR{1,1};
model_cov                   = model_estimation.Covariance;
model_cov_correction        = model_cov/Dt;
model_A                     = model_estimation.Constant;

model_kappa                 = (eye(length(Yem(:,1)))-model_B)/Dt;
[model_eigvec,model_eigval] = eig(model_kappa);
model_eigvec_inv            = model_eigvec^-1;

%mean_estimation : estimated mean reversion mean
mean_estimation           = (eye(length(Yem(:,1)))-model_B)^(-1) * model_A;


if all(imag(diag(model_eigval)) == 0)
    %disp('real');
    for k = 1:length(model_eigvec(1,:))
        if all(abs(model_eigval(k,k)) >= abs(diag(model_eigval))) == 1 
            lincomb_coeff    = model_eigvec_inv(k,:);
        end
    end
else
    for l = 1:length(model_eigvec(1,:))
        %disp('imag');
        if all(abs(imag(model_eigvec_inv(l,:)))<1.0e-8) == 1
            lincomb_coeff    = real(model_eigvec_inv(l,:));
        else
            lincomb_coeff    = zeros(1,length(diag(model_eigval)));
        end
    end
end

standard_deviation              = 0;

for stdnb1 = 1:length(lincomb_coeff)
    for stdnb2 = 1:length(lincomb_coeff)
        standard_deviation      = standard_deviation + lincomb_coeff(stdnb1)*lincomb_coeff(stdnb2)*logmodel_cov_correction(stdnb1,stdnb2);
    end
end



standard_deviation              = sqrt(standard_deviation);

end
