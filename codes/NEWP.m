%CID:01069485

% close all;
clear;

M1                                  = csvread('fangzheng.csv',1,0);
M2                                  = csvread('guangda.csv',1,0);
M3                                  = csvread('huatai.csv',1,0);

Y_Data                              = [M1,M2];
day                                 = 242;
Y_Data                              = Y_Data(1:10*day,:);

iv = 1;

Y_Data = Inter_Data(Y_Data,iv);
%day = 437;

%Yda = [M4];

available                           = length(Y_Data)/day;                        %available days for trading
ini                                 = 0;                                         %initial trading day of the data (0 means start from the beginning of data)
Number_of_Training_Points           = 30;                                        %training day length (15) 62 
Number_of_Testing_Points            = 30;                                        %tesing day length 50

Interval                            = 1;
Comission                           = 0.00018; 

testlength                          = floor((available - ini - Number_of_Training_Points)/Number_of_Testing_Points);   %number of days tested;



gamma =10;

%Remove half day/whole day open/close overlap
for i =1 : available
    Y_Data(i*0.5*day- 2 *(i-1),:)    = [];
    Y_Data(i*day-2*i,:)              = [];
end

lenY = length(Y_Data);


T                                   = 1;                                   %Time horizon setting, Dont Change
N                                   = Number_of_Training_Points;                              %Number of Trading per day, Dont Change
Dt                                  = T/N;                                 %Time interval of trading ,Dont Change


wealthem                            = 0;                                   %initial wealth
plem                                = [];                                  %Trading position for optimal control method
pem                                 = [];
TEST_DATA_VARIANT                   = [];
TRAIN_DATA_VARIANT                  = [];
PROFIT                              = 0;
Coint_val                           = []; 
Coint_tr                            = [];
CA                                  = [];
DA                                  = [];
AA                                  = [];
% figure()
% plot(1:length(Y_Data)-Number_of_Testing_Points,Y_Data(1:(length(Y_Data)-Number_of_Testing_Points),:))
% xlabel('Time in minutes','FontSize',12)
% ylabel('Asset Price','FontSize',12,'Rotation',90);
% legend('601901.SSE','601688.SSE')



h=waitbar(0,'please wait...');
set(h,'doublebuffer','on');
LONG = length(Y_Data);



for Iteration = Number_of_Training_Points+1:Number_of_Testing_Points:length(Y_Data)-Number_of_Testing_Points
    
    train_start         = Iteration - Number_of_Training_Points;
    train_stop          = Iteration - 1;
    
    test_start          = Iteration;
    test_stop           = Iteration + Number_of_Testing_Points -1;
    
    train_data          = Y_Data(train_start:train_stop,:);                 %Assign trainning data
    test_data           = Y_Data(test_start:test_stop,:);                   %Assign trainning data
    
    train_data_variant  = train_data - train_data(1,:);
    TRAIN_DATA_VARIANT  = [TRAIN_DATA_VARIANT;train_data_variant];
    
    test_data_variant   = test_data - test_data(1,:);
    TEST_DATA_VARIANT   = [TEST_DATA_VARIANT;test_data_variant];

    sigma2              = cov(diff(log(train_data),1,1))/(1/(Number_of_Training_Points*iv));
    
%     [CointConst_a,CointPrem_delta,CointVal_alpha,a0]      = ParaEst(train_data');
    [CointConst_a,CointPrem_delta,CointVal_alpha,a0]      =ParaEstRobu(train_data');
    %apple:log cointegration factor
    %delta:estimated cointegration premium (see Duan Pliska 2004��  
    
    if  any(abs(CointPrem_delta)> 3) %1.6
        CointPrem_delta = CointPrem_delta .*0;
    end
    
    DA = [DA CointPrem_delta];
    CA = [CA CointConst_a];
    AA = [AA a0];
    
%     disp('*****************')
%     disp('sigma2 ='),disp(sigma2)
%     disp('omg2 ='),disp(omg2)

    Coint_val                   = [Coint_val a0+(log(test_data)*CointConst_a)'];
    Coint_tr                    = [Coint_tr a0+(log(train_data)*CointConst_a)'];
    
%     [wealthem,plm,piem,alphaem] = ShoA(CointConst_a,a0,CointPrem_delta,test_data',omg2,wealthem,1,Comission,Interval);
    [wealthem,plm,piem,alphaem] = ShoA_Cap(CointConst_a,a0,CointPrem_delta,test_data',sigma2,wealthem,1,Comission,Interval,gamma);    
    [~,~,~,profit]              = ShoAPecCap(CointConst_a,a0,CointPrem_delta,test_data',sigma2,1,Comission,Interval,gamma);
    %wealthem: wealth process of the selected testing days
    %piem: trading position process
    %alphaem: log cointegration factor of the selected testing days
    
    plem                        = [plem plm]; %Trading position of the optimal control method
    pem                         = [pem piem];
    PROFIT                      = [PROFIT PROFIT(end)+ profit];
    
    str=['Loading ',num2str(floor(Iteration*100/length(Y_Data))),'% ...'];
    waitbar(Iteration/length(Y_Data),h,str);
    pause(0.05);
    %disp('delta =');disp(delta)
end

str='Loading Graph... ';
waitbar(1,h,str);
pause(0.05);


% PTA(Yda,day,ini,traini);

ALL = max(sum(abs(pem),1))/100;
figure()
plot(wealthem/ALL)
xlabel('Time in minutes','FontSize',12)
ylabel('Wealth Process/%','FontSize',12,'Rotation',90);


figure()
plot(PROFIT/ALL)
xlabel('Time in minutes','FontSize',12)
ylabel('PROFIT Process/%','FontSize',12,'Rotation',90);

figure()
plot(1:length(pem),pem)
xlabel('Time in minutes','FontSize',12)
ylabel('Trading Postion money','FontSize',12,'Rotation',90);
legend('601901.SSE','601688.SSE')

% figure()
% plot(1:length(plem),plem)
% xlabel('Time in minutes','FontSize',12)
% ylabel('Trading Postion','FontSize',12,'Rotation',90);
% legend('601901.SSE','601688.SSE')

% figure()
% plot(1:length(TEST_DATA_VARIANT),TEST_DATA_VARIANT)
% xlabel('Time in minutes','FontSize',12)
% ylabel('TEST_DATA_VARIANT','FontSize',12,'Rotation',90);
% legend('601901.SSE','601688.SSE')

close(h);
