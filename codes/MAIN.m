%MAIN.m file for variable input
%CID:01069485

%Input Variables
%sigma : volitility
%N     : number of time steps
%T     : Trading Horizon Length
%Yzero : Initial Value of Asset Prices
close all;
clear;
A = [];  %Record alpha estimates
B = [];  %Record delta estimates
for iterloop = 78:78  %Iterate over different random seed
delta           = [1 -0.443 0.220]';  %Initial Delta
a               = [1 -0.468 -0.527]'; %Initial Alpha
a0              = 0;                  %a0 always set to be 0
sigma           = [0.0200 0 0;0.00375 0.01452 0;0.0250 0.00039 0.00967]; %Initial Covariance
T               = 1;                  %Trading Horizon
N               = 2^13;               %Number of trades

Yzero           = transpose([11.10 12.00 11.00]);   %Initial Price

%the main function on simulating Yem
[Yem,~,WinerProc,Dt,L]                        = simufunct(Yzero,delta,a,a0,sigma,N,T,iterloop);% Data Simulation function

%Yem        : Simulated Data using Euler Maruyama
%WinerProc  : Simulated Data with no drift
%Dt         : Interval length
%L          : Length of simulated Data

EstCovar                                      = EstCov(Yem',Dt); %Covariance MAtrix Estimator
[a1,delta1,~,~]                               = ParaEst(Yem);  %parameter estimations for alpha and delta

%a1     : Estimated co-integrating vector a
%delta1 : Estimated delta 



PLOT(Yem,T,WinerProc,a1,a0,L); % Plot Simulated Data + Estimated coint_series


A = [A a1];
B = [B delta1];
end

figure()
plot(1:length(A),A)
xlabel('Iteration Trial','FontSize',12)
ylabel('a','FontSize',12,'Rotation',90);
title('Estimate of vector a')

figure()
plot(1:length(B),B)
xlabel('Iteration Trial','FontSize',12)
ylabel('\delta','FontSize',12,'Rotation',90);
title('Estimate of vector \delta')