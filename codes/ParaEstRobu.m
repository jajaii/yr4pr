function [CointConst_a,CointPrem_delta,CointVal_alpha,a0]=ParaEstRobu(Yem)
%CID:01069485

mm                          = min(size(Yem));
LL                          = max(size(Yem));

log_Data                    = log(Yem);
Observable_Y                = log_Data(1,:)';
DesignMatrix_X              = log_Data(2:mm,:)';


%TT = linspace(0,1,LL).*-TIME; 

%XX = [TT;XX]';

robust_Para                 = robustfit(DesignMatrix_X,Observable_Y,'logistic','1');
% robust_Para                 = robustfit(DesignMatrix_X,Observable_Y,'logistic','1','off');

a0                          = -robust_Para(1);
CointConst_a                = [1;-robust_Para(2:end)];
CointVal_alpha              = (a0 + CointConst_a'*log_Data)';


MeanCointVal_alpha          = mean(CointVal_alpha);

Diff_Y_Data                 = diff(Yem');
Y_Data_Post                 = Yem(:,2:LL);
CointPrem_delta             = mean((Diff_Y_Data'./Y_Data_Post),2)/MeanCointVal_alpha;
CointPrem_delta             = CointPrem_delta ./ CointPrem_delta(1);
end
