function Y_inter = Inter_Data(Yem,Interval)
LL                  = max(size(Yem));
L                   = min(size(Yem));

Num = floor(LL/Interval)+1;

Y_inter =zeros(Num-1,L);


for i = 0: floor(LL/Interval)-1
    Y_inter(i+1,:) = Yem(1+i*Interval,:);

end
end