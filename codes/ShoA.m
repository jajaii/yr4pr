function [wealthem,plm,piem,alphaem] = ShoA(a,a0,delta,Yem,omega,wealth,Dt,commision,Interval)
%CID:01069485
LL                  = max(size(Yem));
L                   = min(size(Yem));

A                   = diag(a);
Gamma               = 8;
Ytemp               = Yem(:,1);
alphatemp           = a0 + a'*log(Ytemp);
piem                = zeros(L,LL);
plm                 = zeros(L,LL);
alphaem             = zeros(1,LL);
alphaem(1)          = alphatemp;
wealthem            = wealth;
% 
% capon = ((commision*0.08*eye(L)./Gamma));


% capon = (commision*0.08*2*eye(L)./Gamma);
capon = 0;

pitemp              = (1/Gamma)*(((omega^(-1))*delta)*alphatemp - a*((delta'*(omega^(-1))*delta)*(LL/Interval*Dt*alphatemp - 0.25*trace(A*omega)*(LL/Interval*Dt)^2)));
Vol_Pos_prior       = pitemp./Ytemp;
% Vol_Pos_prior       = (Vol_Pos_prior./abs(Vol_Pos_prior(1)))*10000;
Proposed_invest     = sum(abs(piem(:,1)));

commission_fee      = Proposed_invest *commision;
wealthem            = [wealthem wealthem(end)-commission_fee]; 
piem(:,1)           = pitemp;
for i = 2:LL
    Ytemp           = Yem(:,i);
    Diff_Y_Data     = Ytemp - Yem(:,i-1);
    
    alphatemp       = a0 + a'*log(Ytemp);
    
    if mod(i-1,Interval) == 0
        pitemp          = (1/Gamma)*(((omega^(-1))*delta)*alphatemp - a*((delta'*(omega^(-1))*delta)*(((LL-i+1)/Interval *Dt)*alphatemp - 0.25*trace(A*omega)*((LL-i+1)/Interval *Dt)^2)));
        Vol_Pos_post    =  pitemp./Ytemp;
%         Vol_Pos_post    = (Vol_Pos_post./abs(Vol_Pos_post(1)))*10000;
        wealthtemp      = ( Vol_Pos_prior-Vol_Pos_post ).*logical((abs(Vol_Pos_prior)-abs(Vol_Pos_post))>0).*(Ytemp - Yem(:,i-1));
        Proposed_invest = sum(abs(Vol_Pos_prior.*Ytemp - pitemp));
        commision_fee   = Proposed_invest*commision;
        wealthtemp      = wealthtemp - commision_fee;
        Vol_Pos_prior   = Vol_Pos_post;
    else
        wealthtemp      = Vol_Pos_prior.*(Ytemp - Yem(:,i-1));
    end
    
    piem(:,i)       = pitemp;
    plm(:,i)        = pitemp./Ytemp;
    wealthem        = [wealthem wealthem(end)+sum(wealthtemp)];
    alphaem(i)      = alphatemp;
end
end