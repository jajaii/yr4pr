mm                              = min(size(Yem));
LL                              = max(size(Yem));

log_Data                        = log(Yem);
Observable_Y                    = log_Data(1,:)';
DesignMatrix_X                  = log_Data(2:mm,:)';

Para                            = (DesignMatrix_X'*DesignMatrix_X)^-1 *(DesignMatrix_X' * Observable_Y);

CointConst_a                    = [1;-Para];
CointVal_alpha                  = (CointConst_a'*log_Data)';



%Covariance Estimation:
%1.log 2.diff 3.cov that 4.divide by delta