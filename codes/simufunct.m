
function [Yem,alphaem,WinerProc,Dt,L] = simufunct(Yzero,delta,a,a0,sigma,N,T,iterloop)
randn('state',iterloop)
%The code incorporate ideas presented in em.m from Higham paper on Euler-Maruyama
%Input Variables
%Yzero : Initial Asset Price 
%sigma : volitility
%N     : number of time steps
%T     : Trading Horizon Length
%dW    : Brownian increments
dt          = T/N;
dW          = sqrt(dt)*randn(length(Yzero),N);
%Terms involved in the ornstein-urenbeck process (mean reversion)
omega       = sigma*sigma';
A           = diag(a);
%R :Step coefficient for Euler-Maruyama Method
%L :Steps in Euler-Maruyama Method
%Dt:Upgraded time step
R           = 1;
Dt          = R*dt;
L           = N/R;
%Yem        : Set of Asset Values in one Trading Horizon
%Ytemp      : Stepwise Asset Value
%alphaem    : Set of Co-integration Factors in one Trading Horizon
%alphatemp  : Stepwise Co-integration Factor
%Winerstep  : Stepwsie no drift data
%WinerProc  : Simulated No drift Data, only depend on increments of Wiener Process
Yem                 = zeros(length(Yzero),L);
Ytemp               = Yzero;
Yem(:,1)            = Yzero;
WinerProc           = zeros(length(Yzero),L);
Winerstep           = Yzero;
WinerProc (:,1)     = Yzero;
alphaem             = zeros(1,L);
alphatemp           = a0 + a'*log(Ytemp);
alphaem(1)          = alphatemp;
for ll = 1:L
    Winc                = sum(dW(:,(R*(ll-1)+1):R*ll),2);
    Ytemp               = Ytemp + (Dt*alphatemp)*(delta.*Ytemp) + sigma * Winc.*Ytemp;
    Winerstep           = Winerstep + sigma * Winc.*Winerstep;
    alphatemp           = a0 + a'*log(Ytemp);
    Yem(:,ll+1)         = Ytemp;
    WinerProc(:,ll+1)   = Winerstep;
    alphaem(ll+1)       = alphatemp;
end
end



