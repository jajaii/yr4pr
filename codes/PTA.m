function PTA(Yem,day,ini,traini)
%CID:01069485
L = length(Yem);
time = ((1:L)/day)+ini+traini;



for i = 1:length(Yem(:,1))
    figure
    plot(time,Yem(i,:));
    xlabel('Time','FontSize',12)
    ylabel('Original Price','FontSize',12,'Rotation',90);
end
end