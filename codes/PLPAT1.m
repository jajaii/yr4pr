function PLPAT1(slark,mm,std,Wtt,bbeta,Yem,day,ini,traini)
L = length(Yem);
ls = length(slark(:,1));
time = ((1:L)/day)+ini+traini;



for i = 1:length(Yem(:,1))
    figure
    yyaxis right
    plot(time,bbeta.*slark(i,:)),hold on
    ylabel('Position','FontSize',12,'Rotation',90);
    yyaxis left
    plot(time,Yem(i,:));
    xlabel('Time','FontSize',12)
    ylabel('Original Price','FontSize',12,'Rotation',90);
end

figure
subplot(2,1,1)
plot(time,Wtt);
xlabel('Time','FontSize',12)
ylabel('Wealth of pairs trading','FontSize',12,'Rotation',90);
title(num2str(traini));

subplot(2,1,2)
plot(time,slark);
xlabel('Time','FontSize',12)
ylabel('linear comb','FontSize',12,'Rotation',90);


figure
yyaxis right
plot(time,bbeta),hold on
ylabel('Position','FontSize',12,'Rotation',90);
yyaxis left
op = sum(slark.*Yem,1);
plot(time,op),hold on
xlabel('Time','FontSize',12)
ylabel('Cointegration factor of pairs trading','FontSize',12,'Rotation',90);

smm = sum(slark.*mm,1);
sdp = smm+std;
sdm = smm-std;
sdsll = smm-2*std;
sdslu = smm+2*std;
plot(time,sdp,'-'),hold on
plot(time,sdm,'-'),hold on
plot(time,sdsll,'-'),hold on
plot(time,sdslu,'-'),hold on
plot(time,smm,'-'),hold off




end